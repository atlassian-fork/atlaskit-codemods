"use strict";

const defineTest = require("jscodeshift/dist/testUtils").defineTest;

defineTest(__dirname, "codemod", null, "jsx-expression");

defineTest(__dirname, "codemod", null, "jsx-conditional");

defineTest(__dirname, "codemod", null, "conditional");

defineTest(__dirname, "codemod", null, "expression");

defineTest(__dirname, "codemod", null, "do-nothing");

defineTest(
  __dirname,
  "codemod",
  null,
  "do-nothing-when-not-conditionally-rendered"
);

defineTest(__dirname, "codemod", null, "do-nothing-when-already-wrapped");
