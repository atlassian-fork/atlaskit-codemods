// @flow

import React, { Component, Fragment } from "react";
import Lorem from "react-lorem-component";

import {
  Spotlight,
  SpotlightManager,
  SpotlightTarget
} from "@atlaskit/onboarding";

export default class SpotlightAutoscrollExample extends Component<*, State> {
  render() {
    const { spotlight } = this.state;
    return (
      <SpotlightManager component={Base}>
        <HighlightGroup>
          <SpotlightTarget name="target-one">
            <Highlight color="red">
              I&apos;m out of view{" "}
              <span role="img" aria-label="sad face">
                😞
              </span>
            </Highlight>
          </SpotlightTarget>
        </HighlightGroup>
        {spotlight !== "off" && (
          <Spotlight
            actions={[
              spotlight === "target-one"
                ? {
                    onClick: this.highlightTwo,
                    text: "Next"
                  }
                : {
                    onClick: this.highlightOne,
                    text: "Prev"
                  },
              { onClick: this.close, text: "Got it" }
            ]}
            dialogPlacement="bottom left"
            heading="Aww, yiss!"
            key={spotlight}
            target={spotlight}
          >
            <Lorem count={1} />
          </Spotlight>
        )}
      </SpotlightManager>
    );
  }
}
