"use strict";

const defineTest = require("jscodeshift/dist/testUtils").defineTest;

defineTest(__dirname, "codemod", null, "basic-transform");

defineTest(__dirname, "codemod", null, "with-identifier");
