// @flow
import React from "react";
import Pagination from "@atlaskit/pagination";

class DummyPagination {
  state = {
    pages: 10
  };

  render() {
    const pages = this.state;
    const { handleChange } = this.props;
    return (
      <Pagination
        pages={[...Array(pages)].map((_, index) => index + 1)}
        onChange={(_, selectedPageIndex) => handleChange(selectedPageIndex)}
      />
    );
  }
}
