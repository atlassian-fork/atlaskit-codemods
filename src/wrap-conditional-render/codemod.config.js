const alwaysTrue = () => true;

const importedFrom = (test = alwaysTrue) => importDeclaration =>
  test(importDeclaration.source.value);

const namedImport = (test = alwaysTrue) => importDeclaration => {
  const [specifier] = importDeclaration.specifiers.filter(
    s => s.imported && test(s.imported.name)
  );
  return specifier ? specifier.local.name : undefined;
};

const defaultImport = (test = alwaysTrue) => importDeclaration => {
  const [specifier] = importDeclaration.specifiers.filter(
    s => s.type === "ImportDefaultSpecifier" && test(s.local.name)
  );
  return specifier ? specifier.local.name : undefined;
};

const component = (specifier, source) => ({
  specifier,
  source,
  both: importDeclaration =>
    specifier(importDeclaration) && source(importDeclaration)
});

// Attempt to transform components described in this array
const components = [
  component(
    namedImport(n => n === "Spotlight"),
    importedFrom(p => p === "@atlaskit/onboarding")
  ),
  component(defaultImport(), importedFrom(p => p.indexOf("modal-dialog") > -1))
];

// For a package name like '@atlaskit/modal-dialog' return the named import to add
const transitionComponentName = packageName =>
  packageName.toLowerCase().indexOf("modal") > -1
    ? "ModalTransition"
    : "SpotlightTransition";

// Alternatively, if you always want the same named import
// const transitionComponentName = () => "ModalTransition";

const options = {
  // components to transform
  components,
  // parser for jscodeshift to use
  parser: "flow",
  // run prettier on output, will respect local prettier config
  prettier: true,
  // named of import to add
  transitionComponentName
};

module.exports = options;
