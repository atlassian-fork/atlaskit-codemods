// @flow
import SmallIcon from "@atlaskit/icon/glyph/file-types/16/file-16-source-code";
import LargerIcon from "@atlaskit/icon/glyph/file-types/24/file-24-gif";

export default () => (
  <div>
    <SmallIcon />
    <LargerIcon />
  </div>
);
