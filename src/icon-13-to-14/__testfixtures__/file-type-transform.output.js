// @flow
import SmallIcon from "@atlaskit/icon-file-type/glyph/source-code/16";
import LargerIcon from "@atlaskit/icon-file-type/glyph/gif/24";

export default () => (
  <div>
    <SmallIcon />
    <LargerIcon />
  </div>
);
