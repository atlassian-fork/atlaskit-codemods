// @flow
import React from "react";
import Pagination from "@atlaskit/pagination";

class DummyPagination {
  state = {
    pages: 10
  };

  render() {
    const pages = this.state;
    const { handleChange } = this.props;
    return <Pagination total={pages} onChange={handleChange} />;
  }
}
