# Atlaskit codemods

This project is a collection of codemods to help upgrading between versions of [Atlaskit](http://atlaskit.atlassian.com/) components.

## Using the codemods

Step one clone this repository into a directory close to where you intend on running the codemods.

```
git clone git@bitbucket.org:atlassian/atlaskit-codemods.git
```

Install the dependencies that this project needs.

```
yarn
```

Install jscodeshift globally.

```
yarn global add jscodeshift
```

Instructions for running each codemod can be found in the README in the directory of the codemod.
